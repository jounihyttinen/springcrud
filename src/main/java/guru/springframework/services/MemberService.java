package guru.springframework.services;


import guru.springframework.domain.Member;

public interface MemberService {
    Iterable<Member> listAllMembers();

    Member getMemberById(Integer id);

    Member saveMember(Member member);

    void deleteMember(Integer id);
}
