package guru.springframework.repositories;

import guru.springframework.domain.Member;
import org.springframework.data.repository.CrudRepository;

public interface MemberRepository extends CrudRepository<Member, Integer>{
}
