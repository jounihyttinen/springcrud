package guru.springframework.bootstrap;

import guru.springframework.domain.Member;
import guru.springframework.repositories.MemberRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class MemberLoader implements ApplicationListener<ContextRefreshedEvent> {

    private MemberRepository memberRepository;

    private Logger log = Logger.getLogger(MemberLoader.class);

    @Autowired
    public void setMemberRepository(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Member hyttjo = new Member();
        hyttjo.setFirstName("Jouni");
        hyttjo.setLastName("Hyttinen");
        memberRepository.save(hyttjo);

        log.info("Saved Shirt - id: " + hyttjo.getId());

        Member meima = new Member();
        meima.setFirstName("Matti");
        meima.setLastName("Meikäläinen");
        memberRepository.save(meima);

        log.info("Saved Mug - id:" + meima.getId());
    }
}
